var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// definimos objeto schema
var bicicletasSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        createIndexes: {
            type: '2dsphere',
            sparse: true
        }
    }
});

// instanciar nuevo objeto
bicicletasSchema.statics.createInstance = function (code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};


bicicletasSchema.methods.toString = function() {
    return 'code: ' + this.code + '| color: ' + this.color;
}


// encontrar todas las bicicletas
bicicletasSchema.statics.allBicis = function(cb) {
    return this.find({}, cb);
}

// agregar una bicicleta
bicicletasSchema.statics.add = function(aBici, cb) {
    this.create(aBici, cb);
}

// encontrar una bicicleta por su codigo
bicicletasSchema.statics.findByCode = function(aCode, cb) {
    return this.findOne({code: aCode}, cb);
}


// encontrar una bicicleta por su id
bicicletasSchema.statics.findIt = function(aId, cb) {
    return this.findOne({_id: aId}, cb);
}


// bicicletasSchema.statics.update = function(aId, cb) {
//     return this.save({_id: aId}, cb);
// }

// borrar una bicicleta por su codigo
bicicletasSchema.statics.removeByCode = function(aCode, cb) {
    return this.deleteOne({code: aCode}, cb);
}

// borrar una bicicleta por su id
bicicletasSchema.statics.removeIt = function(aId, cb) {
    return this.deleteOne({_id: aId}, cb);
}


module.exports = mongoose.model('Bicicleta', bicicletasSchema);



// // definimos objeto
// var Bicicleta = function (id, color, modelo, ubicacion) {
//     this.id = id;
//     this.color = color;
//     this.modelo = modelo;
//     this.ubicacion = ubicacion;
// }

// Bicicleta.prototype.toString = function () {
//     return `id: ${this.id} | color: ${this.color}`;
// }

// // array de bicicletas
// Bicicleta.allBicis = [];

// // agregar bicicleta
// Bicicleta.add = function (aBici) {
//     Bicicleta.allBicis.push(aBici);
// }

// // buscar bicicleta por ID
// Bicicleta.findById = function (aBiciId) {
//     var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
//     if (aBici)
//         return aBici;
//     else
//         throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
// }

// // borrar bicicleta
// Bicicleta.removeById = function (aBiciId) {
//     // var aBiciId = Bicicleta.findById(aBiciId);
//     for (var i = 0; i < Bicicleta.allBicis.length; i++) {
//         if (Bicicleta.allBicis[i].id == aBiciId) {
//             Bicicleta.allBicis.splice(i, 1);
//             break;
//         }
//     }
// }

// // creamos 2 bicicletas para tener objetos con los cuales trabajar
// var a = new Bicicleta(1, 'rojo', 'urbana', [-34.893667, -56.149109]);
// var b = new Bicicleta(2, 'blanca', 'urbana', [-34.893500, -56.154956]);

// // las agregamos al array
// // Bicicleta.add(a);
// // Bicicleta.add(b);

// module.exports = Bicicleta;