var map = L.map('main_map').setView([-34.880649, -56.157229], 13);

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
    maxZoom: 18
}).addTo(map);


// L.marker([-34.893667, -56.149109]).addTo(map);
// L.marker([-34.893500, -56.154956]).addTo(map);

$(document).ready( function() {

    console.log('ready');

    return $.ajax({
        type: 'GET',
        dataType: "html",
        url: "http://localhost:3000/api/bicicletas/json",
        success: function (result) {
            let data = JSON.parse(result);
            // console.log(JSON.parse(result));
            data.Bicicletas.forEach(bici => {
                L.marker(bici.ubicacion, {
                    title: bici.code
                }).addTo(map);
            });
        },
        error: function(req, err){ console.log('my message' + err); }
    })


});

